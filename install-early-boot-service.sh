#!/bin/bash


echo "[Unit]
Description=Early Boot Service
DefaultDependencies=no
Before=sysinit.target

[Service]
Type=oneshot
ExecStart=/usr/bin/cat /proc/uptime" > /usr/lib/systemd/system/early-boot-service.service

/sbin/restorecon -v /usr/lib/systemd/system/early-boot-service.service
cd /usr/lib/systemd/system/sysinit.target.wants/
ln -s ../early-boot-service.service

mkdir -p /usr/lib/dracut/modules.d/00early-boot-service
echo '#!/usr/bin/bash

install() {
    inst_multiple -o \
      "$systemdsystemunitdir"/early-boot-service.service \
      "$systemdsystemunitdir"/sysinit.target.wants/early-boot-service.service
}' > /usr/lib/dracut/modules.d/00early-boot-service/module-setup.sh

dracut -f
rm -f /usr/lib/systemd/system/sysinit.target.wants/early-boot-service.service

# Need to find out the new size by having it initially fail the first attempt. the `-u` command 
# does not auto resize.
SIZE=$(abootimg -u /boot/aboot-$1.img -r /boot/initramfs-$1.img 2>&1 | awk -F'[( ]' '/updated is too big for the Boot Image/ {print $11}')

if [ -z "$SIZE" ]; then
  abootimg -u "/boot/aboot-$1.img" -r "/boot/initramfs-$1.img" -c "bootsize=$SIZE"
fi

# These following commands are used for ostree images
# aboot-update $1
# aboot-deploy -d /dev/disk/by-partlabel/boot_a aboot-$1.img
# reboot

