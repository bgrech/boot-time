from bokeh.plotting import figure, save
from bokeh.models import ColumnDataSource, HoverTool, LabelSet
from bokeh.io import output_file
import copy


def build_chart(
    timings, test_date, test_config, system_config, basename, sample_number
):
    # Parallel list data structure to be used by Bokeh ColumDataSource
    data = {
        "id": [],
        "start": [],
        "duration": [],
        "end": [],
        "name": [],
        "label": [],
        "log_source": [],
        "color": [],
        "bar_height": [],
    }

    # Copy of the data structure to hold the sorted lists
    sorted_data = copy.deepcopy(data)

    # Standard visualization formatting
    dmesg_color = "blue"
    systemd_color = "red"
    standard_bar_height = 1
    chart_width = 1000
    chart_height = 700

    # Highlight formatting for KPIs
    highlight_color = "green"
    highlight_bar_height = 2

    # Parse the boot-time sample data structure for metrics of interest
    for item in timings:
        data["start"].append(round(item["activating"] / 1000, 3))
        data["duration"].append(round(item["time"] / 1000, 3))
        data["end"].append(round((item["activating"] + item["time"]) / 1000, 3))
        data["name"].append(item["name"])
        if "activated" in item:
            data["log_source"].append("systemd")
        else:
            data["log_source"].append("dmesg")
        # TODO -- Add KPI highlighting
        if "kpi" in item and item["kpi"]:
            data["color"].append(highlight_color)
            data["bar_height"].append(highlight_bar_height)
            data["label"].append(item["name"])
        else:
            if "activated" in item:
                data["color"].append(systemd_color)
            else:
                data["color"].append(dmesg_color)
            data["bar_height"].append(standard_bar_height)
            data["label"].append(None)

    # Parallel-sort the data structure lists based on start time
    (
        sorted_data["start"],
        sorted_data["duration"],
        sorted_data["end"],
        sorted_data["name"],
        sorted_data["label"],
        sorted_data["log_source"],
        sorted_data["color"],
        sorted_data["bar_height"],
    ) = (
        list(t)
        for t in zip(
            *sorted(
                zip(
                    data["start"],
                    data["duration"],
                    data["end"],
                    data["name"],
                    data["label"],
                    data["log_source"],
                    data["color"],
                    data["bar_height"],
                )
            )
        )
    )

    # Enumerate the log items sequentially after sort
    for id, n in enumerate(sorted_data["start"]):
        sorted_data["id"].append(id)

    # Get total number of actions
    total_actions = len(sorted_data["id"])

    # Convert the data into a ColumnDataSource for Bokeh
    source = ColumnDataSource(data=sorted_data)

    # Configure the tool-tip display on mouse hover
    hover = HoverTool(
        tooltips=[
            ("Source", "@log_source"),
            ("Name", "@name"),
            ("Started", "@start{0.00} ms"),
            ("Ended", "@end{0.00} ms"),
            ("Run Time", "@duration{0.00} ms"),
        ],
        point_policy="follow_mouse",
    )

    # Build the Bokeh chart
    p = figure(
        title=f"""Boot Time Measurements -- {total_actions} Actions

        Date: {test_date}
        IP: {test_config['IPaddr']}
        Release: {system_config['osrelease']}
        Kernel: {system_config['kernel']}
        """,
        y_axis_label="Boot Action (Sequence ID)",
        x_axis_label="Time Since Start (ms)",
        width=chart_width,
        height=chart_height,
    )
    p.hbar(
        y="id",
        left="start",
        right="end",
        source=source,
        color="color",
        height="bar_height",
    )
    p.tools.append(hover)
    p.y_range.flipped = True
    p.title.text_font_size = "15pt"
    p.axis.major_label_text_font_size = "15pt"
    p.axis.axis_label_text_font_size = "15pt"

    # Label the highlighted items
    labels = LabelSet(
        x="start",
        y="id",
        text="label",
        x_offset=5,
        y_offset=5,
        source=source,
        text_color=highlight_color,
        background_fill_color="white",
        border_line_color=highlight_color,
        border_line_width=1,
    )
    p.add_layout(labels)

    output_filename = f"{basename}-sample_{sample_number}-chart.html"
    output_file(filename=output_filename)
    save(p)

    return f"Created Bokeh chart at {output_filename}"
